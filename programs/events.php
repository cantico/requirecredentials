<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/functions.php';

/**
 * before page created
 *
 * @param	bab_eventBeforePageCreated 	$event
 */
function requirecredentials_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    if (bab_isUserLogged()) {
        return;
    }

    if (requirecredentials_matchException('tg=login&cmd=authform')) {
        return;
    }

    if (requirecredentials_matchException('tg=login&cmd=signon')) {
        return;
    }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/requirecredentials/');

    $exceptions = (string) $registry->getValue('exceptions');

    $enabled = $registry->getValue('enabled', true);
    $choose = $registry->getValue('choose', true);


    if (!$enabled) {
        return;
    }


    $arr = preg_split('/\R/', $exceptions);
    foreach($arr as $exceptions) {
        if (!empty($exceptions) && requirecredentials_matchException($exceptions)) {
            return;
        }
    }

    if (!$choose) {
        return bab_requireCredential();
    }


    $dialog = requirecredentials_getDialog();

    if (!isset($dialog)) {
        return bab_requireCredential();
    }

    $addon = bab_getAddonInfosInstance('requirecredentials');
    $babBody = bab_getBody();


    $babBody->addStyleSheet($addon->getStylePath().'main.css');
    $babBody->addJavascriptFile($addon->getTemplatePath().'main.js', true);
    $babBody->babecho($dialog);


}


/**
 * Get dialog as html
 * @return string
 */
function requirecredentials_getDialog()
{
    $icons = bab_functionality::get('Icons');
    /*@var $icons Func_Icons */
    $icons->includeCss();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/requirecredentials/');

    $widgets = bab_Widgets();
    $frame = $widgets->Frame('requirecredentials-authtypes', $widgets->VBoxLayout()->setVerticalSpacing(2, 'em'))
        ->addClass('requirecredentials-authtypes')
        ->setMetadata('modal', $registry->getValue('modal'));
    $frame->setTitle(requirecredentials_translate('Choose your authentication method'));


    $frame->addItem(
        $widgets->Html($registry->getValue('tophtml'))
        ->addClass('tophtml')
    );


    $defaultauth = bab_functionality::get('PortalAuthentication');
    /*@var $defaultauth Func_PortalAuthentication */




    $others = $widgets->Frame(null, $widgets->FlowLayout())->addClass('requirecredentials-others');
    $others->addClass(Func_Icons::ICON_LEFT_16);


    $list = bab_functionality::getFunctionalities('PortalAuthentication');

    if (1 === count($list)) {
        return null;
    }

    foreach ($list as $authType) {

        $auth = bab_functionality::get('PortalAuthentication/'.$authType);
        /*@var $auth Func_PortalAuthentication */

        if (!$auth) {
            continue;
        }
        
        $link = $widgets->Link(
            $widgets->Icon($auth->getMenuName(), Func_Icons::APPS_PREFERENCES_AUTHENTICATION),
            '?tg=login&cmd=signon&sAuthType='.substr($authType, 4)
        );

        if ($auth instanceof $defaultauth) {
            $defaultAuthType = $authType;
            continue;
        }

        $others->addItem($link);
    }

    $frame->addItem(
        $widgets->Link(
            $widgets->Icon($defaultauth->getMenuName(), Func_Icons::APPS_PREFERENCES_AUTHENTICATION),
            '?tg=login&cmd=signon&sAuthType='.substr($defaultAuthType, 4)
        )->addClass('requirecredentials-default')->addClass(Func_Icons::ICON_LEFT_48)
    );


    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/requirecredentials/');


    $frame->addItem(
        $widgets->Label(
            sprintf(
                requirecredentials_translate('You will be redirected to default authentication page in %ds'),
                $registry->getValue('timeout', 3)
            )
        )
        ->addClass('timer')
    );


    $frame->addItem(
        $widgets->RichText($registry->getValue('bottomtext'))
        ->addClass('bottomtext')
    );

    $frame->addItem($others);

    return $frame->display($widgets->HtmlCanvas());
}



/**
 * Test exception
 * @return bool
 */
function requirecredentials_matchException($querystring)
{
    if (!isset($_SERVER['QUERY_STRING']) || !empty($_POST)) {
        return true;
    }

    if (false !== mb_strpos($_SERVER['QUERY_STRING'], $querystring)) {
        return true;
    }
    
    if ('' === $_SERVER['QUERY_STRING'] && ($querystring === '/' || $querystring === '?')) {
        return true;
    }

    return false;
}
