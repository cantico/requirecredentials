��          �            h  !   i  E   �  E   �  6     %   N     t     �  N   �  !   �     	     %  .   *  <   Y     �  [  �  '   �  T   "  I   w  ?   �  ,        .  #   >  a   b  3   �     �     
  ;     =   R     �                                       
                               	          Choose your authentication method Content to display before the authenticate with default method button Do not redirect to the login form if the url contain one of the lines Fallback to default authentication method method after List of exceptions (one url per line) Modal dialog Options has been saved Propose the authentication methods before authenticate with the default method Require credentials for all pages Require credentials options Save Text to display before the alternative methods You will be redirected to default authentication page in %ds seconds Project-Id-Version: requirecredentials
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-10-21 11:12+0200
PO-Revision-Date: 2016-10-21 11:13+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: requirecredentials_translate;requirecredentials_translate:1,2
X-Generator: Poedit 1.8.9
X-Poedit-SearchPath-0: programs
 Choix de la méthode d'authentification Contenu à afficher avant le bouton pour s'authentifier avec la méthode par défaut Ne pas rediriger vers l'authentification si l'url contient une des lignes Basculer vers la méthode d'authentification par défaut après Liste des exceptions (une adresse par ligne) Fenêtre modale Les options ont été enregistrées Proposer le choix des méthodes d'authentification avant de basculer vers la méthode par défaut Demander une authentification pour toutes les pages Options du module Enregistrer Texte à afficher avant la liste des méthodes alternatives Vous serez redirigé vers la page d'authentification dans %ds secondes 